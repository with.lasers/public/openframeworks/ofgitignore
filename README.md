# ofGitIgnore

A `.gitignore` file for OpenFrameworks, based on the example in [ofBook][1] and formatted correctly.



[1]:https://openframeworks.cc/ofBook/chapters/version_control_with_git.html
